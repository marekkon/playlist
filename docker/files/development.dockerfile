FROM python:3.8.3-slim-buster
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN groupadd -g 1000 playlist && useradd -d /home/playlist -s /bin/bash -m -u 1000 -g 1000 playlist \
    && apt-get -y update && apt-get -y install authbind \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && touch /etc/authbind/byport/80 \
    && chmod 500 /etc/authbind/byport/80 \
    && chown playlist:playlist /etc/authbind/byport/80
    

USER playlist
COPY --chown=playlist:playlist . /home/playlist/app/src
WORKDIR /home/playlist/app/src
ENV PATH="/home/playlist/.local/bin:$PATH"

RUN pip install --upgrade pip \
    && pip install pip-tools wheel \
    && pip-sync requirements/prod.txt requirements/dev.txt \
    && pip install -e .

