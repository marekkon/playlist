from setuptools import find_packages, setup

import playlist

setup(
    name='playlist',
    version=playlist.__version__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
        'SQLAlchemy',
        'Flask-SQLAlchemy',
        'pytz',
        'pyyaml',
        'requests',
    ],
    entry_points={
        'console_scripts': [
            'playlist=cli:cli'
        ]
    },
)
