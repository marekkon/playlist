import pytest

from playlist.source import sources, get_sources


@pytest.mark.unit
def test_get_sources_without_params():
    result = get_sources()
    assert result == sources


@pytest.mark.unit
def test_get_sources_with_select():
    data = [
        {'slug': 'youtube', 'name': 'Youtube'}
    ]

    result = get_sources(select=True)

    assert result == data
