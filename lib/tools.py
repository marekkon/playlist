from datetime import datetime
import imp
import os
import pytz


def tz_datetime():
    '''Return timezone aware datetime'''

    return datetime.now(pytz.utc)


def import_source_module(source, module_type):
    f, path, desc = imp.find_module(module_type, [os.path.join(os.environ['BASE_DIR'], 'playlist', 'source', source)])
    return imp.load_module(source, f, path, desc)
