import os

from cli.utils import setenv, configure_app
from playlist.app import create_app


setenv('APPLICATION_CONFIG', 'development')


configure_app(os.getenv('APPLICATION_CONFIG'))
app = create_app(os.getenv('FLASK_CONFIG'))
