========
Playlist
========
This application collects music information from a variety of sources. The application has two purposes:

1. enable more advanced filtering of resources that are not possible on the source sites,
2. creating playlists (manually or automatically) based on the above filters.

The application is in the early stages of development. Currently it supports youtube.

There are no plans to launch the app publicly at the moment. For this reason, it does not contain any user authentication or user authorization functionality.
But there is nothing to prevent this application from being run locally.

Installation
------------
Apart from python and additional libraries, the application requires a docker and docker-compose (additional requirements may come in the future). Alternatively, you can run the application without a docker, but you will need to configure additional services manually.

Installation and launch of the application (recommended method):

1. Create a directory
2. In the directory execute :code:`git clone https://gitlab.com/marekkon/playlist.git`
3. Create virtual environment, e.g.: :code:`python -m venv venv` and activate it :code:`. venv/bin/activate` (bash/zsh)
4. Execute :code:`pip install -e .`
5. Create docker network: :code:`docker network create apps`
6. Copy config file :code:`cp config/app/development.example.yml config/app/development.yml` and fill it with your data:

  - leave :code:`app->name` as is
  - to enter the `ip address` you need:

    - execute :code:`docker network inspect -f '{{range .IPAM.Config}}{{.Subnet}}{{end}}' apps`, something like this will appear: :code:`172.17.0.0/24`
    - change the above numbers, e.g.: :code:`172.17.0.[x]', where x is between 2 and 254 and remove :code:`/xx` part

  - enter some random text to :code:`secret_key` or generate it using, e.g.: :code:`python -c 'import os; print(os.urandom(16))'`
  - to get :code:`youtube_api_key` you must generate it at https://developers.google.com/youtube/ (gmail account required)

7. Execute :code:`playlist compose up -d` and then :code:`playlist db --init`
8. Run the web browser and enter above ip adress, e.g.: http://172.17.0.2

