const apiEndpoint = '/api_v1/';
const thumpPath = '/static/thumb/';

Vue.component('source-select', {
  props: {
    sources: Array
  },
  template: `
    <form method="get" class="frontal">
      <label for="source">Source:</label>
      <div class="custom-select">
        <select @change="$emit('change', $event.target.value)" id="source" name="source">
          <option value="all" selected>All sources</option>
          <option v-for="source in sources" :value="source.slug">{{ source.name }}</option>
        </select>
        <span class="custom-select-arrow"></span>
      </div>
    </form>
  `
});

Vue.component('channels', {
  props: {
    source: String
  },
  data() {
    return {
      channels: []
    };
  },
  watch: {
    source: async function(newVal, oldVal) {
      if (newVal !== oldVal) {
        this.fetchChannels(newVal)
      }
    }
  },
  created: async function() {
    this.fetchChannels('all')
  },
  methods: {
    fetchChannels: async function(source) {
      try {
        const response = await fetch(`${apiEndpoint}channel?source=${source}`);
        const data = await response.json();
        this.channels.splice(0, this.channels.length, ...data);
      } catch (err) {
        // Display error box
      }
    }
  },
  template: `
    <section class="channel-list">
      <ul v-if="channels.length">
        <channel v-for="channel in channels" :key="channel.id" :channel="channel"></channel>
      </ul>
      <div v-else class="box-info">
        <p>No elements to display...</p>
      </div>
    </section>
  `,
});

Vue.component('channel', {
  props: {
    channel: Object
  },
  computed: {
    imgSrc: function() {
      return `${thumpPath}${this.channel.id}_default.jpg`;
    },
    description: function() {
      let desc = this.channel.description.split('\\n').filter((s) => s.length > 0).map((s) => s.trim())[0]
      const maxDescLength = 199;
      if (desc.length > maxDescLength) {
        const nextSpace = desc.indexOf(' ', maxDescLength);
        desc = desc.substring(0, nextSpace) + '...';
      }
      return desc;
    },
    tags: function() {
      return ['house', 'future house'];
    }
  },
  methods: {
    removeChannel: function(event) {
      console.log('element removed');   
    }
  },
  template: `
    <li class="channel-item">
      <img :src="imgSrc" class="channel-item--thumb" />
      <div class="channel-item--main">
        <h3>{{ channel.name }}</h3>
        <dl class="channel-item--stats">
          <dt>Tracks:</dt>
          <dd>{{ channel.music_count }}</dd>
          <dt>Followers:</dt>
          <dd>{{ channel.follower_count }}</dd>
        </dl>
        <div v-if="channel.description.length" v-html="description" class="channel-item--desc"></div>
        <dl class="channel-item--tags">
          <dt>Tags:</dt>
          <dd v-for="tag in tags" :key="tag" class="tag">{{ tag }}</dd>
        </dl>
      </div>
      <ul class="channel-item--menu">
        <li><a :href="'/channel/' + this.channel.id">View</a></li>
        <li><a href="#" @click="removeChannel">Remove</a></li>
      </ul>
    </li>
  `
});

const vm = new Vue({
  el: '#vm',
  data: {
    sources: [],
    source: undefined
  },
  created: function() {
    this.sources.splice(0, 0, ...data);
  },
  methods: {
    setSource(value) {
      this.source = value;
    }
  }
})
