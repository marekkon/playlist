const apiEndpoint = '/api_v1/';
const thumpPath = '/static/thumb/';

Vue.component('channel-search-form', {
  props: {
    sources: Array
  },
  data() {
    return {
      source: '',
      query: ''
    };
  },
  methods: {
    submitQuery: function(event) {
      if (this.source && this.query) {
        this.$emit('search', {source: this.source, query: this.query});
      }
      // TODO: Add some info if one of them is missing
    }
  },
  template: `
    <form method="get" class="frontal search">
      <label for="source">Source:</label>
      <div class="custom-select">
        <select v-model="source" id="source" name="source">
          <option value="" selected>Select source...</option>
          <option v-for="source in sources" :value="source.slug">{{ source.name }}</option>
        </select>
        <span class="custom-select-arrow"></span>
      </div>
      <input type="text" name="query" class="search-input" v-model="query" @keyup.enter="submitQuery" />
      <button type="button" class="search-button" @click="submitQuery">
        <svg viewBox="0 0 128 128" class="icon">
          <use href="#icon-search"></use>
        </svg>
      </button>
    </form>
  `
});

Vue.component('channel-search-list', {
  props: {
    request: Object
  },
  data() {
    return {
      channels: [],
      source: '',
      totalResults: 0,
      pageNumber: -1,
      nextPageToken: null,
      prevPageToken: null,
    };
  },
  watch: {
    request: {
      handler: function(newVal, oldVal) {
        if (newVal != oldVal) {
          this.searchChannels();
        }
      },
      deep: true
    }
  },
  methods: {
    searchChannels: async function() {
      let r = this.request;
      const response = await fetch(`${apiEndpoint}channel/search?source=${r.source}&query=${r.query}`);
      const data = await response.json();
      this.totalResults = data.total_results;
      this.pageNumber = data.page;
      this.nextPageToken = data.next_page;
      this.prevPageToken = data.prev_page;
      this.source = r.source
      this.channels.splice(0, this.channels.length, ...data.channels);
    }
  },
  template: `
    <section class="channel-list">
      <ul v-if="channels.length">
        <channel v-for="channel in channels" :key="channel.id" :channel="channel" :source="source"></channel>
      </ul>
      <div v-else class="box-info">
        <p>No elements to display...</p>
      </div>
    </section>
  `,
});

Vue.component('channel', {
  props: {
    channel: Object,
    source: String
  },
  computed: {
    description: function() {
      let desc = this.channel.description.split('\\n').filter((s) => s.length > 0).map((s) => s.trim())[0]
      const maxDescLength = 199;
      if (desc.length > maxDescLength) {
        const nextSpace = desc.indexOf(' ', maxDescLength);
        desc = desc.substring(0, nextSpace) + '...';
      }
      return desc;
    },
    tags: function() {
      return ['house', 'future house'];
    }
  },
  methods: {
    addChannel: async function() {
      const settings = {
          method: 'POST',
          body: JSON.stringify({id: this.channel.id, source: this.source }),
          headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
          }
      };
      try {
          const response = await fetch(`${apiEndpoint}channel/add`, settings);
          const data = await response.json();
          console.log(response);
          console.log(data);
          // return data;
      } catch (e) {
          console.log(e);
          // return e;
      }
    }
  },
  template: `
    <li class="channel-item">
      <img :src="channel.thumbnail" class="channel-item--thumb" />
      <div class="channel-item--main">
        <h3>{{ channel.name }}</h3>
        <div v-if="channel.description.length" v-html="description" class="channel-item--desc"></div>
      </div>
      <ul class="channel-item--menu">
        <li><a :href="'/channel/' + this.channel.id + '?preview=true'">Preview</a></li>
        <li><a href="#" @click="addChannel">Add</a></li>
      </ul>
    </li>
  `
});

const vm = new Vue({
  el: '#vm',
  data: {
    sources: [],
    request: {}
  },
  created: function() {
    this.sources.splice(0, 0, ...data);
  },
  methods: {
    setSearchData(value) {
      this.request = value
    }
  }
})
