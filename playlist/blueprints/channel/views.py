from flask import Blueprint, render_template

from playlist.source import get_sources

channel = Blueprint('channel', __name__, template_folder='templates')


@channel.route('/channel')
def index():
    return render_template('channel/index.html', sources=get_sources(True))


@channel.route('/channel/search')
def search():
    return render_template('channel/search.html', sources=get_sources(True))
