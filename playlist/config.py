import os


class Config(object):
    '''Base configuration'''

    SQLALCHEMY_DATABASE_URI = 'sqlite:////' + os.path.join(os.getenv('BASE_DIR'), 'playlist.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    '''Production configuration'''


class DevelopmentConfig(Config):
    '''Development configuration'''


class TestingConfig(Config):
    '''Testing configuration'''

    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(os.getenv('BASE_DIR'), 'playlist_test.db')
    TESTING = True
