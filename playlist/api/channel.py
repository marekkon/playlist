from flask import escape, jsonify, request, current_app
from http import HTTPStatus


from playlist.api import api
from playlist.db.channel import Channel
from playlist.source import process_external_data, save_thumbnails, sources
from lib.tools import import_source_module


@api.route('/channel')
def get_channels():
    response_code = HTTPStatus.NOT_FOUND
    channels = []
    source = escape(request.args.get('source', '').strip())

    if source not in sources and source != 'all':
        return jsonify(channels), response_code

    if source == 'all':
        channels = Channel.query.all()
    else:
        channels = Channel.query.filter_by(source=source).all()

    if channels:
        response_code = HTTPStatus.OK

    return jsonify(channels), response_code


@api.route('/channel/search')
def search_channels():
    response_code = HTTPStatus.NOT_FOUND
    response = []
    source = escape(request.args.get('source', '').strip())
    query = escape(request.args.get('query', '').strip())

    if source not in sources or not query:
        return jsonify(response), response_code

    request_module = import_source_module(source, module_type='request')
    response_data, response_code = request_module.search(query)

    response_module = import_source_module(source, module_type='response')
    response_data = process_external_data(
        response_data,
        response_code,
        response_processor=response_module.SearchResponse,
        error_processor=response_module.ErrorResponse,
        search_type='channel'
    )

    return jsonify(response_data), response_code


@api.route('/channel/add', methods=['POST'])
def add_channel():
    response_code = HTTPStatus.NOT_FOUND
    response = {'message': 'There is no such channel'}
    channel_id = escape(request.json.get('id', '').strip())
    source = escape(request.json.get('source', '').strip())

    if not channel_id or source not in sources:
        return jsonify(response), response_code

    request_module = import_source_module(source, module_type='request')
    response_data, response_code = request_module.get_channels((channel_id,))

    response_module = import_source_module(source, module_type='response')
    response_data = process_external_data(
        response_data,
        response_code,
        response_processor=response_module.ChannelResponse,
        error_processor=response_module.ErrorResponse
    )

    if 'id' in response_data:
        Channel(**response_data).save()
        save_thumbnails(response_data)
        response['message'] = 'Channel added successfully'
    else:
        response['message'] = response_data

    return jsonify(response), response_code
