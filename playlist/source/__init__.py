from http import HTTPStatus
import mimetypes
import os
import requests

from playlist.db.channel import Channel

sources = {
    'youtube': {
        'name': 'Youtube',
    },
}
RESULTS_PER_PAGE = 25


def get_sources(select=False):
    result = sources

    if select:
        result = [{'slug': key, 'name': value['name']} for key, value in sources.items()]

    return result


def process_external_data(data, status_code, response_processor, error_processor, **kwargs):
    if status_code >= HTTPStatus.BAD_REQUEST:
        proc = error_processor(data)
    else:
        proc = response_processor(data, status_code, **kwargs)

    return proc.process()


def save_thumbnails(subject):
    thumbnails_path = os.path.join(os.getenv('BASE_DIR'), 'playlist', 'static', 'thumb')
    thumbnails = {key: value for key, value in subject.items() if key.startswith('thumbnail')}
    subject_id = subject.get('id')

    for key, url in thumbnails.items():
        response = requests.get(url)

        size = key.split('_')[1]
        content_type = response.headers['content-type']
        extension = mimetypes.guess_extension(content_type)
        thumbnail_name = os.path.join(thumbnails_path, f'{subject_id}_{size}{extension}')

        with open(thumbnail_name, "wb") as f:
            f.write(response.content)
