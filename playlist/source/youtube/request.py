import os
import requests

from playlist.source import RESULTS_PER_PAGE

api_key = os.getenv('YOUTUBE_API_KEY')
request_types = {
    'search': {
        'url': 'https://www.googleapis.com/youtube/v3/search?',
        'part': 'snippet',
    },
    'list': {
        'url': 'https://www.googleapis.com/youtube/v3/channels?',
        'part': ['snippet', 'statistics', 'brandingSettings'],
    },
}


def search(query, search_type='channel', results_number=RESULTS_PER_PAGE):
    payload = {
        'q': query,
        'maxResults': results_number,
        'part': request_types['search']['part'],
        'type': search_type,
        'key': api_key,
    }

    response = requests.get(request_types['search']['url'], params=payload)
    return response.json(), response.status_code


def get_channels(channel_ids, results_number=RESULTS_PER_PAGE):
    payload = {
        'id': channel_ids,
        'maxResults': results_number,
        'part': request_types['list']['part'],
        'key': api_key,
    }

    response = requests.get(request_types['list']['url'], params=payload)
    return response.json(), response.status_code
