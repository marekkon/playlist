from abc import ABC, abstractmethod
from http import HTTPStatus

from playlist.source import RESULTS_PER_PAGE


class Response(ABC):

    def __init__(self, data):
        self.data = data
        super().__init__()

    @abstractmethod
    def process(self):
        pass


class SearchResponse(Response):

    def __init__(self, data, status_code, **kwargs):
        self.status_code = status_code
        self.search_type = kwargs.get('search_type', 'channel')
        super().__init__(data)

    def process(self):
        result = {}
        result['total_results'] = self.data.get('pageInfo', {}).get('totalResults')

        result['next_page'] = self.data.get('nextPageToken')
        result['prev_page'] = self.data.get('prevPageToken')

        if result['total_results'] <= RESULTS_PER_PAGE:
            result['page'] = -1
        elif not result['prev_page']:
            result['page'] = 1
        elif not result['next_page']:
            result['page'] = (result['total_results'] // RESULTS_PER_PAGE) + 1  # last
        else:
            result['page'] = 0  # TODO: set proper page number

        result['channels'] = []
        for item in self.data['items']:
            channel = {
                'id': item.get('id', {}).get(self.search_type + 'Id'),
                'published_at': item.get('snippet', {}).get('publishedAt'),
                'name': item.get('snippet', {}).get('title'),
                'description': item.get('snippet', {}).get('description'),
                'thumbnail': item.get('snippet', {}).get('thumbnails', {}).get('default', {}).get('url'),
            }
            result['channels'].append(channel)

        return result


class ChannelResponse(Response):

    def __init__(self, data, status_code):
        self.status_code = status_code
        super().__init__(data)

    def process(self):
        result = {}
        page_info = self.data.get('pageInfo', {})
        results = page_info.get('totalResults') or page_info.get('resultsPerPage')

        if results and results > 1:
            result['next_page'] = self.data.get('nextPageToken')
            result['prev_page'] = self.data.get('prevPageToken')

            if result['results'] <= RESULTS_PER_PAGE:
                result['page'] = -1
            elif not result['prev_page']:
                result['page'] = 1
            elif not result['next_page']:
                result['page'] = (result['results'] // RESULTS_PER_PAGE) + 1  # last
            else:
                result['page'] = 0  # TODO: set proper page number

            result['channels'] = []
            for item in self.data['items']:
                channel = {
                    'id': item.get('id'),
                    'source': 'youtube',
                    'name': item.get('snippet', {}).get('title'),
                    'description': item.get('snippet', {}).get('description'),
                    'thumbnail_default': item.get('snippet', {}).get('thumbnails', {}).get('default', {}).get('url'),
                    'thumbnail_large': item.get('snippet', {}).get('thumbnails', {}).get('high', {}).get('url'),
                    'follower_count': item.get('statistics', {}).get('subscriberCount'),
                    'music_count': item.get('statistics', {}).get('videoCount'),
                }
                result['channels'].append(channel)
        else:
            items = self.data.get('items', [])
            if items:
                item = items[0]
                result = {
                    'id': item.get('id'),
                    'source': 'youtube',
                    'name': item.get('snippet', {}).get('title'),
                    'description': item.get('snippet', {}).get('description'),
                    'thumbnail_default': item.get('snippet', {}).get('thumbnails', {}).get('default', {}).get('url'),
                    'thumbnail_large': item.get('snippet', {}).get('thumbnails', {}).get('high', {}).get('url'),
                    'follower_count': item.get('statistics', {}).get('subscriberCount'),
                    'music_count': item.get('statistics', {}).get('videoCount'),
                }

        return result


class ErrorResponse(Response):

    def process(self):
        return {
            'message': self.data.get('error', {}).get('message')
        }
