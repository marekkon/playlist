from flask import Flask

from playlist.api import api
from playlist.blueprints.main import main
from playlist.blueprints.channel import channel

from playlist.db import db


def create_app(config_name):
    app = Flask(__name__)

    config_module = f'playlist.config.{config_name.capitalize()}Config'

    app.config.from_object(config_module)

    app.register_blueprint(main)
    app.register_blueprint(channel)
    app.register_blueprint(api, url_prefix='/api_v1')

    init_extensions(app)

    return app


def init_extensions(app):
    db.init_app(app)
