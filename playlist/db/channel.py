from dataclasses import dataclass
from playlist.db import db
from playlist.db.model import DateTimeMixin, ResourceMixin
from playlist.db.tag import tags_channels as tags


@dataclass
class Channel(DateTimeMixin, ResourceMixin, db.Model):
    __tablename__ = 'channels'

    id: str
    source: str
    name: str
    description: str
    follower_count: int
    music_count: int

    id = db.Column(db.String(64), primary_key=True)
    source = db.Column(db.String(32), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text)
    follower_count = db.Column(db.Integer)
    music_count = db.Column(db.Integer, nullable=False, default=0)
    music = db.relationship('Music', backref='channel', lazy=True)
    tags = db.relationship('Tag', secondary=tags, lazy='subquery', backref=db.backref('channels', lazy=True))

    def __init__(self, **channel):
        self.id = channel.get('id')
        self.source = channel.get('source')
        self.name = channel.get('name')
        self.description = channel.get('description')
        self.follower_count = channel.get('follower_count')
        self.music_count = channel.get('music_count')

    def __repr__(self):
        return f'<Channel {self.name}>'
