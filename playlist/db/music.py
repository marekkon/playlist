from lib.tools import tz_datetime
from playlist.db import db
from playlist.db.tag import tags_music as tags


class Music(db.Model):
    __tablename__ = 'music'

    id = db.Column(db.String(64), primary_key=True)
    channel_id = db.Column(db.String(64), db.ForeignKey('channels.id'), nullable=False)
    artist = db.Column(db.String(255))
    title = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text)
    url = db.Column(db.String(255))
    published_at = db.Column(db.DateTime(timezone=True), default=tz_datetime)
    view_count = db.Column(db.Integer, nullable=False, default=0)
    rating = db.Column(db.Float)
    duration = db.Column(db.Integer)
    region_restriction = db.Column(db.JSON)
    status = db.Column(db.Integer)
    removed = db.Column(db.Boolean, nullable=False, default=False)
    tags = db.relationship('Tag', secondary=tags, lazy='subquery', backref=db.backref('music', lazy=True))

    def __repr__(self):
        name = self.artist + ' - ' + self.title if self.artist else self.title
        return f'<Music {name}>'
