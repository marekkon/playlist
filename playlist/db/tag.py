from playlist.db import db


tags_channels = db.Table(
    'tags_channels',
    db.Column('tag_id', db.Integer, db.ForeignKey('tags.id'), primary_key=True),
    db.Column('channel_id', db.String(64), db.ForeignKey('channels.id'), primary_key=True)
)

tags_music = db.Table(
    'tags_music',
    db.Column('tag_id', db.Integer, db.ForeignKey('tags.id'), primary_key=True),
    db.Column('music_id', db.String(64), db.ForeignKey('music.id'), primary_key=True)
)


class Tag(db.Model):
    __tablename__ = 'tags'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)

    def __repr__(self):
        return f'<Tag {self.name}>'
