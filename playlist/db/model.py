from dataclasses import dataclass
from datetime import datetime
from lib.tools import tz_datetime
from playlist.db import db


@dataclass
class DateTimeMixin(object):
    created_at: datetime
    updated_at: datetime

    created_at = db.Column(db.DateTime(timezone=True), default=tz_datetime)
    updated_at = db.Column(db.DateTime(timezone=True), default=tz_datetime, onupdate=tz_datetime)


class ResourceMixin(object):

    def save(self):
        """
        Save a model instance.

        :return: Model instance
        """
        db.session.add(self)
        db.session.commit()

        return self
