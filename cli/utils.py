import os
import signal
import subprocess
import yaml

import click


base_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
parent_dir = os.path.realpath(os.path.join(base_dir, '..'))
APP_CONFIG_PATH = os.path.join(base_dir, 'config', 'app')
DOCKER_PATH = os.path.join(base_dir, 'docker')


def setenv(variable, default):
    '''Ensure an environment variable exists and has a value.'''
    os.environ[variable] = os.getenv(variable, default)


def app_config_file(config):
    return os.path.join(APP_CONFIG_PATH, f'{config}.yml')


def docker_compose_file(config):
    return os.path.join(DOCKER_PATH, f'{config}.yml')


def configure_app(config):
    with open(app_config_file(config)) as f:
        raw_config_data = yaml.load(f, Loader=yaml.BaseLoader)

    setenv('BASE_DIR', base_dir)
    setenv('PARENT_DIR', parent_dir)

    config_data = {}
    for key, value in raw_config_data.items():
        if isinstance(value, dict):
            prefix = key + '_'
            for sub_key, sub_value in value.items():
                config_data[(prefix + sub_key).upper()] = sub_value
        else:
            config_data[key.upper()] = value

    for key, value in config_data.items():
        setenv(key, value)


def docker_compose_cmdline(commands_string=None):
    config = os.getenv('APPLICATION_CONFIG')
    configure_app(config)
    app_name = os.getenv('APP_NAME')

    compose_file = docker_compose_file(config)

    if not os.path.isfile(compose_file):
        raise ValueError(f'The file {compose_file} does not exist')

    command_line = [
        'docker-compose',
        '-p',
        app_name + '_' + config,
        '-f',
        compose_file,
    ]

    if commands_string:
        command_line.extend(commands_string.split(" "))

    return command_line
