import os
import signal
import subprocess

import click
from cli.utils import (
    setenv,
    configure_app,
    docker_compose_cmdline
)
from cli.db import init_db


setenv('APPLICATION_CONFIG', 'development')


@click.group()
def cli():
    pass


@cli.command(context_settings={'ignore_unknown_options': True})
@click.argument('subcommand', nargs=-1, type=click.Path())
def flask(subcommand):
    configure_app(os.getenv('APPLICATION_CONFIG'))

    cmdline = ['flask'] + list(subcommand)

    try:
        p = subprocess.Popen(cmdline)
        p.wait()
    except KeyboardInterrupt:
        p.send_signal(signal.SIGINT)
        p.wait()


@cli.command(context_settings={'ignore_unknown_options': True})
@click.argument('subcommand', nargs=-1, type=click.Path())
def compose(subcommand):
    cmdline = docker_compose_cmdline() + list(subcommand)

    try:
        p = subprocess.Popen(cmdline)
        p.wait()
    except KeyboardInterrupt:
        p.send_signal(signal.SIGINT)
        p.wait()


@cli.command()
@click.argument('filenames', nargs=-1)
def test(filenames):
    os.environ['APPLICATION_CONFIG'] = 'testing'
    configure_app(os.getenv('APPLICATION_CONFIG'))

    cmdline = docker_compose_cmdline('up -d')
    subprocess.call(cmdline)

    cmdline = ['pytest', '-svv', '--cov=application', '--cov-report=term-missing']
    cmdline.extend(filenames)
    subprocess.call(cmdline)

    cmdline = docker_compose_cmdline('down')
    subprocess.call(cmdline)


@cli.command()
@click.option('--init', is_flag=True)
def db(init):
    if init:
        init_db()
