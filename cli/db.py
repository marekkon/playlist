from flask.cli import with_appcontext

from playlist.db import db
from playlist.db.channel import Channel
from playlist.db.music import Music
from playlist.db.tag import Tag


@with_appcontext
def init_db():
    db.drop_all()
    db.create_all()
